package main

import (
	"encoding/gob"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"os"

	"bitbucket.org/holmberg556/go/exec_data"
)

func main() {
	verbose := flag.Bool("verbose", false, "be more verbose")
	flag.Parse()

	if *verbose {
		fmt.Printf("This is exec_client ...\n")
	}

	if len(flag.Args()) < 2 {
		log.Fatalf("Usage: exec_client [--verbose] HOST:PORT EXE [ARG1 .. ARGn]\n")
	}
	args := flag.Args()
	remote := args[0]
	exe_name := args[1]
	exe_args := args[2:]

	exe, err := ioutil.ReadFile(exe_name)
	if err != nil {
		log.Fatal(err)
	}

	server, err := net.Dial("tcp", remote)
	if err != nil {
		log.Fatal(err)
	}
	enc := gob.NewEncoder(server)

	request := exec_data.Request{
		Image: exe,
		Args:  exe_args,
	}
	err = enc.Encode(request)
	if err != nil {
		log.Fatal("encode error:", err)
	}

	dec := gob.NewDecoder(server)
	var response exec_data.Response
	err = dec.Decode(&response)
	if err != nil {
		log.Fatal("decode error:", err)
	}
	for _, output := range response.Output {
		fmt.Printf("%s", string(output))
	}
	os.Exit(response.ExitStatus)
}
