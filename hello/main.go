package main

import (
	"fmt"

	"bitbucket.org/holmberg556/go/world"
)

func main() {
	fmt.Printf("%s %s\n", hello_msg(), world.Msg())
}

func hello_msg() string {
	return "Hello"
}
