package main

import (
	"encoding/gob"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"os"
	"os/exec"
	"syscall"

	"bitbucket.org/holmberg556/go/exec_data"
)

func main() {
	verbose := flag.Bool("verbose", false, "be more verbose")
	flag.Parse()

	server, err := net.Listen("tcp", ":7777")
	if err != nil {
		log.Fatal(err)
	}
	accept_loop(server, *verbose)
}

func accept_loop(server net.Listener, verbose bool) {
	for {
		conn, err := server.Accept()
		if err != nil {
			log.Fatal(err)
		}
		handleConnection(conn, verbose)
	}
}

func handleConnection(conn net.Conn, verbose bool) {
	defer conn.Close()
	if verbose {
		fmt.Println("got connection")
	}

	exe_program := "exe_program"

	dec := gob.NewDecoder(conn)
	var request exec_data.Request
	err := dec.Decode(&request)
	if err != nil {
		log.Fatal("decode error:", err)
	}

	program_bytes := request.Image
	if _, err = os.Stat(exe_program); os.IsExist(err) {
		err = os.Remove(exe_program)
		if err != nil {
			log.Fatal("Remove error:", err)
		}
	}

	err = ioutil.WriteFile(exe_program, program_bytes, 0755)
	if err != nil {
		log.Fatal("WriteFile error:", err)
	}

	program := "./" + exe_program
	args := request.Args
	cmd := exec.Command(program, args...)

	output, err := cmd.CombinedOutput()

	var response exec_data.Response
	if err == nil {
		response.ExitStatus = 0
	} else if exit_error, ok := err.(*exec.ExitError); ok {
		wait_status := exit_error.Sys().(syscall.WaitStatus)
		response.ExitStatus = wait_status.ExitStatus()
	} else {
		log.Fatal("CombinedOutput error:", err)
	}
	response.Output = output

	enc := gob.NewEncoder(conn)
	err = enc.Encode(response)
	if err != nil {
		log.Fatal("encode error:", err)
	}
}
