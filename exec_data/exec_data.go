package exec_data

type Request struct {
	Image []byte
	Args  []string
}

type Response struct {
	Output     []byte
	ExitStatus int
}
